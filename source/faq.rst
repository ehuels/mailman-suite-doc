=================================
Frequenty Asked Questions (Admin)
=================================

Frequently asked questions by Mailman 3 administrators.

.. contents::
   :depth: 4
   :local:

1. Confirmation emails to Users has wrong domain name (example.com!)
--------------------------------------------------------------------

This happens when your reverse (SSL) proxy isn't setting up the correct headers
when proxying requests. Fix this by setting the right ``proxy_set_header``
directives::

    # For Nginx.
    location / {
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-Host $host;
        proxy_pass http://localhost:8000/;
    }

Appropriate headers for different web servers needs to be set if something
other than Nginx is being used.


2. The domain name displayed in Hyperkitty shows example.com or something else.
-------------------------------------------------------------------------------

The name of the domain comes from the :setting:`django:SITE_ID` which should be
to set to the domain you want to display. If you have Postorius >= 1.3.3 The
Domains view will show you the :setting:`django:SITE_ID` for the domain and has
an `Edit` link to the Django admin UI entry for that site.

Otherwise, you can go to the Django admin `Sites` view (``/admin`` or
``/<prefix>/admin``) and see the DOMAIN NAME and DISPLAY NAME for each defined
site, and you can edit the domain name.

Note that setting `SITE_ID = 0` in Django's settings will cause HyperKitty to
display the DISPLAY NAME for the domain whose DOMAIN NAME matches the accessing
domain.  However, do not set `SITE_ID = 0` in a new installation without any
existing `Sites` as this will cause an issue in applying migrations. Only set
`SITE_ID = 0` after there are domains defined in the Django admin `Sites` view.


3. How to enable debug logging in Mailman Core?
------------------------------------------------

There are a couple of loggers in Mailman Core which can be individually cofigured
with log levels. Available loggers are:

- archiver        --  All archiver output
- bounce          --  All bounce processing logs go here
- config          --  Configuration issues
- database        --  Database logging (SQLAlchemy and Alembic)
- debug           --  Only used for development
- error           --  All exceptions go to this log
- fromusenet      --  Information related to the Usenet to Mailman gateway
- http            --  Internal wsgi-based web interface
- locks           --  Lock state changes
- mischief        --  Various types of hostile activity
- plugins         --  Plugin logs
- runner          --  Runner process start/stops
- smtp            --  SMTP activity
- subscribe       --  Information about leaves/joins
- vette           --  Message vetting information

You can set their log levels by adding the following to your ``mailman.cfg``::

    [logging.http]
    level: DEBUG


4. How to print logs to standard out instead of regular files in Mailman Core?
-------------------------------------------------------------------------------

For each of the available loggers (listed in FAQ #3), you can set the ``path`` 
for each logger separately in ``mailman.cfg``::

   [logging.root]
   path: /dev/stdout


5. What is Mailman-web?
-----------------------

Mailman web is basically a set of configuration files for deploying Mailman's
web frontend packaged in a single Python package. It provides a command line,
``mailman-web`` which can be used to run administrative commands.

If you are familiar with Django, or have seen older versions of this
documentation, ``mailman-web`` command is equivalent of running
``django-admin --pythonpath /etc/mailman3/ --settings settings``. It just makes
it simpler to run administrative commands and simplify user documentation.

See `documentation for Mailman-web <https://mailman-web.readthedocs.io/>`_.

6. How to disable signup?
-------------------------

To disable signup/registartion for new users, while continuing to allow signed
up users to login, you can add the following to your ``settings.py``::


  # /etc/mailman3/settings.py
  ACCOUNT_ADAPTER = 'django_mailman3.views.user_adapter.DisableSignupAdapter'


To disable only social account signups, but keep the signups open, you can add
the following to your ``settings.py``::

  # /etc/mailman3/settings.py
  SOCIALACCOUNT_ADAPTER = 'django_mailman3.views.user_adapter.DisableSocialSignupAdapter'

You would need **django-mailman3 >= 1.3.6** for these settings to work.

7. How to change language in Mailman 3?
---------------------------------------

TODO

