Optimize Mailman Setup
======================

Mailman 3 Suite is a complicated setup but also comes with several swappable
parts to suite the use case of various types of users who have 10 lists to
those who have 100,000 lists.

Optimize Mailman Core's API
---------------------------

Mailman Core uses falcon_ web framework for the REST API. By default, when you
install Mailman from `pip`, you will get the pre-built wheels using pure
Python. However, you can significantly improve performance of Falcon using
either PyPy_ or `Cpython with Cython`_. This does not require *any* changes in
the application itself, but only requires you to install Falcon alongwith
Cython or use a python interpreter other than CPython.


.. _falcon: https://falcon.readthedocs.io/en/stable/
.. _PyPy: https://falcon.readthedocs.io/en/stable/user/install.html#pypy
.. _Cpython with Cython: https://falcon.readthedocs.io/en/stable/user/install.html#cpython
