=================
The Mailman Suite
=================

The Mailman home page is http://www.list.org, and there is a community driven
wiki at http://wiki.list.org.

The Mailman Suite consists of 5 individual projects. Below are links to
documentation for each of the projects, their issue trackers and latest releases.

Those packages are copyrighted by the `Free Software Foundation`_ and
distributed under the terms of the `GNU General Public License (GPL) version
3`_ or later.


.. raw:: html

   <style>
   table, th, td {
     border: 1px solid black;
     padding: 7px;
   }
   table {
     width: 100%;
     text-aligh: center;
   }
   </style>
   <table>
   <tr>
   <th>Project</th>
   <th>Description</th>
   <th>Issues</th>
   <th>Docs</th>
   <th>Release</th>
   </tr>
   <tr>
   <td>Mailman Core</td>
   <td>The mailing list manager core</td>
   <td><a href="https://gitlab.com/mailman/mailman/issues">Issues</a></td>
   <td><a href="https://docs.mailman3.org/projects/mailman/en/latest/">Docs</a></td>
   <td><a href="https://pypi.org/project/mailman/"><img src="https://img.shields.io/pypi/v/mailman.svg" /></a></td>
   </tr>
   <tr>
   <td>Postorius</td>
   <td>The web user interface</td>
   <td><a href="https://gitlab.com/mailman/postorius/issues">Issues</a></td>
   <td><a href="https://docs.mailman3.org/projects/postorius/en/latest/">Docs</a></td>
   <td><a href="https://pypi.org/project/postorius/"><img src="https://img.shields.io/pypi/v/postorius.svg" /></a></td>
   </tr>
   <tr>
   <td>Hyperkitty</td>
   <td>The web archiver</td>
   <td><a href="https://gitlab.com/mailman/hyperkitty/issues">Issues</a></td>
   <td><a href="https://docs.mailman3.org/projects/hyperkitty/en/latest/">Docs</a></td>
   <td><a href="https://pypi.org/project/hyperkitty/"><img src="https://img.shields.io/pypi/v/hyperkitty.svg" /></a></td>
   </tr>
   <tr>
   <td>Mailmanclient</td>
   <td>The official REST API python bindings</td>
   <td><a href="https://gitlab.com/mailman/mailmanclient/issues">Issues</a></td>
   <td><a href="https://docs.mailman3.org/projects/mailmanclient/en/latest/">Docs</a></td>
   <td><a href="https://pypi.org/project/mailmanclient/"><img src="https://img.shields.io/pypi/v/mailmanclient.svg" /></a></td>
   </tr>
   <tr>
   <td>Hyperkitty Mailman plugin</td>
   <td>Archiver plugin for Core</td>
   <td><a href="https://gitlab.com/mailman/mailman-hyperkitty/issues">Issues</a></td>
   <td><a href="https://docs.mailman3.org/projects/hyperkitty/en/latest/install.html#connecting-to-mailman">Docs</a></td>
   <td><a href="https://pypi.org/project/mailman-hyperkitty/"><img src="https://img.shields.io/pypi/v/mailman-hyperkitty.svg" /></a></td>
   </tr>
   <tr>
   <td>Django-mailman3</td>
   <td>Django utilities for Web UI</td>
   <td><a href="https://gitlab.com/mailman/django-mailman3/issues">Issues</a></td>
   <td><a href="https://gitlab.com/mailman/django-mailman3/-/blob/master/README.rst">Docs</a></td>
   <td><a href="https://pypi.org/project/django-mailman3/"><img src="https://img.shields.io/pypi/v/django-mailman3.svg" /></a></td>
   </tr>
   <tr>
   </tr>
   </table>

.. note::
   There are inconsistencies throughout this documentation in examples which
   invoke the Django management commands. The actual invocation depends on
   how Django and Mailman 3 are installed. It may be

   - mailman-web ...
   - python manage.py ...
   - django-admin ...

   or something else. When reading these docs, you may need to translate one
   of the above into what works in your installation.


The Pre-Installation Guide
==========================

What do I need to know before trying to install Mailman3?

..  toctree::
    :maxdepth: 2

    architecture.rst
    pre-installation-guide.rst

The Installation Guide
======================

..  toctree::
    :caption: Installation
    :maxdepth: 1

    django-primer.rst
    install/install.rst
    upgrade-guide.rst
    faq.rst

Configuring Mailman 3
=====================

Mailman 3 can be configured in a wide variety of ways. After you have installed
Mailman 3, you can now proceed to configure it for production use.

.. toctree::
   :caption: Configuration
   :maxdepth: 2

   config-core.rst
   config-web.rst

Migrating to Mailman 3 from Mailman 2.1
=======================================

Migrating from Mailman 2.1 to Mailman 3.x

.. toctree::
   :caption: Migrating from 2.1
   :maxdepth: 2

   migration.rst
   faq-migration.rst


The User Guide
==============


..  toctree::
    :caption: User Guide
    :maxdepth: 2

    userguide.rst

Documentation for Mailman 3 List Owners and Site Administrators is not yet
complete.

.. The Community Guide
   ===================


The Contributor Guide
=====================

..  toctree::
    :caption: Contributor Guide
    :maxdepth: 2

    translation.rst
    devsetup.rst
    documentation.rst


.. _Free Software Foundation: http://www.fsf.org/
.. _GNU General Public License (GPL) version 3: http://www.gnu.org/licenses/quick-guide-gplv3.html
